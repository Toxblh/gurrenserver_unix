//
//  main.cpp
//  Server_BinaryChat
//
//  Created by Toxblh on 05.02.15.
//  Copyright (c) 2015 Toxblh. All rights reserved.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#define NUM_CLIENTS		10
#define PORT					1021
#define BUFFER				1024
#define LEN_NICK			255
#define PATH_TO_LOG		"log.txt"

void * ThClient(void *arg);
//void * ThWriteLog(void *arg);

FILE*			outLog;

int           client_socket; //Сокет для клиента
sockaddr_in		client_address; //Адрес клиента
int						NClient = 0,
ClientsOnline = 0;
char					lastMess[50];

/*
 Описания таблицы в 3х переменных
 __________________________________
 |_nick___|_ip____|_sock__|_live__|
 |toxbh   | 1.1.1 | 132   | 1     | 1 клиент toxblh
 |vasya   | 1.2.3 | 54    | 0     | вышел - можно занять.
 |artacer | 2.2.2 | 164   | 1     | 1 клиент artacer
 |artacer | 2.2.3 | 172   | 1     | 2 клиент artacer
 |________|_______|_______|_0_____| Пустая ячейка

 */
char			ClientNickIp[NUM_CLIENTS][2][LEN_NICK];
int 			ClientSock[NUM_CLIENTS],
					ClientLive[NUM_CLIENTS];

/*
	Добавляет в список нового пользователя
	Возвращает id клиента в списке
 */
int NewClient(char* nick, char* ip, int sock) {
    int		number_client = 0,
    i;
    bool	inLive = false;

    //Проверить нет ли пустых пустых
    for (i = 0; i < NClient; i++) {
        if (ClientLive[i] == 0) {
            int m;
            for (m = 0; m < (int)strlen(nick); m++) {//set Nick
                ClientNickIp[i][0][m] = nick[m];
            }

            for (int m = 0; m < (int)strlen(ip); m++) {//set IP
                ClientNickIp[i][1][m] = ip[m];
            }
            ClientSock[i] = sock;
            ClientLive[i] = 1;
            inLive = true;
            number_client = i;
            ClientsOnline++;
            break;
        }
    }

    //Если не нашли места. Добавляем новое
    if (!inLive) {
        if (ClientLive[NClient] == 1)
            NClient++;

        //Проверка, что не заполнили весь массив клиентов
        if (NClient < NUM_CLIENTS) {
            for (i = 0; i < (int)strlen(nick); i++) {
                ClientNickIp[NClient][0][i] = nick[i];
            }

            for (int i = 0; i < (int)strlen(ip); i++) {
                ClientNickIp[NClient][1][i] = ip[i];
            }
            ClientSock[NClient] = sock;
            ClientLive[NClient] = 1;
            number_client = NClient;
            ClientsOnline++;
        }
        else {
            return 0;
        }
    }

    return number_client;
}

//Суммирование строк
char* sumstr(char* first, const char* second) {
    int flen = (int)strlen(first);//sizeof(first)-1;
    int slen = (int)strlen(second);//sizeof(second)-1;
    int allLen = flen + slen;
    //printf("First: %s; Second %s; LenF: %d; LenS: %d; LenAll: %d; SLF: %lu; SLS: %lu", first, second, flen, slen, allLen, strlen(first),strlen(second));

    char* out = new char[allLen + 1];

    for (int i = 0; i <= allLen; i++) {
        if (i < flen) {
            out[i] = first[i];
        }
        else {
            out[i] = second[i - flen];
        }
    }

    out[allLen + 1] = '\0';

    return out;
}


//Удалить клиента
int DeleteClient(int id) {
    ClientsOnline--;
    //Если id больше Кол-ва клиент ничего не делать
    if (id > NClient) {
        return 0;
    }
    //Если пользователь уже удален тоже вылететь
    if (ClientLive[id] == 0) {
        return 0;
    }
    //Обнуляем жизнь пользователю
    ClientLive[id] = 0;
    ClientSock[id] = 0;
    memset(ClientNickIp[id][0], 0, LEN_NICK);
    memset(ClientNickIp[id][1], 0, LEN_NICK);

    close(ClientSock[id]);

    return 0;
}

//Показать онлайн на сервере
void ViewTable() {
    printf("\n         USERS ONLINE: %d\n|_nick________|_ip_______________|_sock___|_live__|\n", ClientsOnline);
    //fopen_s(&outLog, PATH_TO_LOG, "a+");
    outLog = fopen(PATH_TO_LOG, "a+");
    fprintf(outLog, "\n         USERS ONLINE: %d\n|_nick________|_ip_______________|_sock___|_live__|\n", ClientsOnline);
    for (int i = 0; i <= NClient; i++) {
        printf("| %-10s  | %-15s  | %-5d  | %-4d  |\n", ClientNickIp[i][0], ClientNickIp[i][1], (int)ClientSock[i], ClientLive[i]);
        fprintf(outLog, "| %-10s  | %-15s  | %-5d  | %-4d  |\n", ClientNickIp[i][0], ClientNickIp[i][1], (int)ClientSock[i], ClientLive[i]);
    }
    fprintf(outLog, "\n");
    fclose(outLog);
    printf("\n");
}

// 0 - Сообщение; 1 - Онлайн лист; 9 - Приветствие от клиента.
void SendOnlineList(char* OnlineList) {
    OnlineList[0] = '1';
    int x = 1;
    for (int i = 0; i <= NClient; i++) {
        if (ClientLive[i] == 1) {
            char* Adres = ClientNickIp[i][1];
            int lenAdr = sizeof(Adres)-1;
            for (int j = 0; j < lenAdr; j++) {
                OnlineList[x] = Adres[j];
                x++;
            }
            OnlineList[x] = '\n';
            x++;
        }
    }
    OnlineList[x] = '\0';
    //printf("%s", OnlineList);
}

/*
 std::wstring AnsiToUtf8(const std::string &csAnsiStr) {
 INT iLen = ::MultiByteToWideChar(CP_ACP, 0, csAnsiStr.c_str(), -1, NULL, 0);

 if (iLen > 0) {
 std::vector<wchar_t> vw(iLen);
 ::MultiByteToWideChar(CP_ACP, 0, csAnsiStr.c_str(), -1, &vw[0], iLen);

 return &vw[0];
 }
 else {
 return L"";
 }
 }
 */

void write_log(char* str_log) {
    char* log = sumstr(str_log, "\n");
    printf("%s", log);
    outLog = fopen(PATH_TO_LOG, "w");
    fprintf(outLog, "%s", log);
    fclose(outLog);
}


int main(int argc, char* argv[]) {
    //setlocale(LC_ALL, ".OCP");
    //setlocale(LC_ALL, "Russian");

    int listener;
    struct sockaddr_in	sockIn;

    // Готовим сокет на приём
    write_log((char*)"Ready socket");

    //Открываем слушателя
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if(listener < 0)
    {
        perror("sock");
        return 1;
    }

    //Настройка дырки
    sockIn.sin_family = AF_INET;
    sockIn.sin_port = htons(PORT);
    sockIn.sin_addr.s_addr = INADDR_ANY;

    // Привязываем к порту и открываем его собственно
    if (bind(listener, (struct sockaddr *)&sockIn, sizeof(sockIn)) < 0) {
        //    if (bind(sock, (struct sockaddr *)&sockIn, sizeof(sockIn)) < 0) {
        perror("bind");
        return 2;
    }

    // Слушаем порт, очередь 8
    listen(listener, 8);

    write_log((char*)"Wait conection...");

    int client_socket;
    sockaddr_in client_address;
    int client_addres_size = sizeof(client_address);

    while ((client_socket = accept(listener, (sockaddr*)&client_address, (socklen_t*)&client_addres_size))) {
        //Размерность адреса
        //
        //client_socket = accept(sock, (sockaddr *)&client_address, &client_addres_size)
        //client_socket = accept(sock, &client_address, &client_addres_size)
        //client_socket = accept(sock, NULL, NULL)

        printf("Accepted client: %s:%d\n", inet_ntoa(client_address.sin_addr), ntohs(client_address.sin_port));
        outLog = fopen(PATH_TO_LOG, "a+");
        fprintf(outLog, "Accepted client: %s:%d\n", inet_ntoa(client_address.sin_addr), ntohs(client_address.sin_port));
        fclose(outLog);

        char buff[512];
        if (recv(client_socket, buff, sizeof(buff), 0) < 0) {
            perror("recv");
            break;
        }

        printf("%s\n", buff);
        outLog = fopen(PATH_TO_LOG, "a+");
        fprintf(outLog, "%s\n", buff);
        fclose(outLog);
        //delete buff;

        if (buff[0] == '9') {
            char nick[255];
            int i;
            for (i = 1; i < (int)strlen(buff) + 1; i++) {
                nick[i - 1] = buff[i];
            }
            nick[i] = '\0';
            printf("nick: %s\n", nick);
            outLog = fopen(PATH_TO_LOG, "a+");
            fprintf(outLog, "nick: %s\n", nick);
            fclose(outLog);

            NewClient(nick, inet_ntoa(client_address.sin_addr), client_socket);

            ViewTable();

            if (ClientsOnline > 0) {
                char online[] = "0System: New connect!\n";
                //printf("%d user online\n", ClientsOnline);
                outLog = fopen(PATH_TO_LOG, "a+");
                fprintf(outLog, "%d user online\n", ClientsOnline);
                fclose(outLog);

                char OnlineList[1024];
                SendOnlineList(OnlineList);
                for (i = 0; i <= NClient; i++) {
                    if (ClientLive[i] == 1) {
                        send(ClientSock[i], online, strlen(online) + 1, 0);
                        send(ClientSock[i], OnlineList, strlen(OnlineList) + 1, 0);
                    }
                }
            }

            else {
                printf("No User on line\n");
            }

            int id_th[10], result;
            pthread_t thread[10];
            result = pthread_create(&thread[NClient], NULL, ThClient, &id_th);
            if (result != 0) {
                printf("Создание второго потока");
                return 3;
            }
        }
        else  {
            close(client_socket);
        }
    }

    close(client_socket);
    return 0;

}

// Эта функция создается в отдельном потоке
// и обсуживает очередного подключившегося клиента независимо от остальных

//DWORD WINAPI ThClient(LPVOID ID)
void * ThClient(void *arg)
{
    int 	mySocket;
    char 	buff[BUFFER],
    myNick[20];
    int 	myID = * (int*) arg,//(int)ID,
    //loc_id = * (int*) arg,
    ret = 0,
    i = 0;

    mySocket = ClientSock[myID];
    memset(&buff, 0, 1024);
    //Записали адрес свой в переменную
    strcpy(myNick, ClientNickIp[myID][0]);

    while (1) {
        if (recv(mySocket, buff, BUFFER, 0) < 0)
        {
            perror("recv");
            break;
        }

        buff[ret-1] = '\0';

        printf("Mess from [%s]: %s", myNick, buff);
        outLog = fopen(PATH_TO_LOG, "a+");
        fprintf(outLog, "Mess from [%s]: %s", myNick, buff);
        fclose(outLog);

        time_t current_time;
        struct tm time_info;
        char timeString[6];
        time(&current_time);
        time_info = *localtime(&current_time);
        strftime(timeString, 6, "%H:%M", &time_info);
        //localtime(&time_info, &current_time);

        if (strcmp(lastMess, myNick) != 0) {
            for (i = 0; i <= NClient; i++) {
                if (ClientLive[i] == 1) {
                    char* BMC = sumstr((char*)"0", sumstr(timeString,sumstr(sumstr((char*)" [", (char*)myNick),sumstr((char*)"]: ",buff))));
                    if (send(ClientSock[i], BMC, strlen(BMC) + 1, 0) == 0) {
                        delete BMC;
                        break;
                    }
                    else {
                        perror("send");
                        delete BMC;
                        break;
                    }

                    /*
                     std::wstring a = AnsiToUtf8(buff);
                     wprintf(L"%s", a.c_str());
                     printf("%s", (char*)a.c_str());
                     fopen_s(&outLog, PATH_TO_LOG, "a+");
                     fwprintf(outLog, L"Mess from [%s]: %s", myNick, a.c_str());
                     fclose(outLog);*/
                }
            }
        }
        else {
            for (i = 0; i <= NClient; i++) {
                if (ClientLive[i] == 1) {
                    char* BMC = sumstr((char*)"0", sumstr(timeString, sumstr((char*)": ", buff)));
                    if (send(ClientSock[i], BMC, strlen(BMC) + 1, 0) == 0){
                        DeleteClient(i);
                    }
                    else
                    {
                        perror("send");
                        close(ClientSock[i]);
                        break;
                    }
                }
            }
        }

        strcpy(lastMess, myNick);
        memset(&buff, 0, 1024);
    }

    DeleteClient(myID);

    write_log((char*)"Disconect\n");

    if (NClient > 0) {
        char OnlineList[1024];
        SendOnlineList(OnlineList);
        for (i = 0; i <= NClient; i++) {
            if (ClientLive[i] == 1) {
                if (send(ClientSock[i], OnlineList, sizeof(OnlineList), 0))
                    break;
                else
                {
                    printf("send() failed 216: \n");
                    break;
                }
            }
        }
    }

    ViewTable();

    return 0;
}


/*
 //DWORD WINAPI ThWriteLog(LPVOID MSG) {
 void * ThWriteLog(void *arg) {
 char* Message = (char*) arg;
 outLog = fopen(PATH_TO_LOG, "a+");
 while (outLog){
 fprintf(outLog, (char*)Message);
 fclose(outLog);
 }
 return 0;
 }
 */
